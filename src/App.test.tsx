import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders CICD text", () => {
  render(<App />);
  const linkElement = screen.getByText(/CI/i);
  expect(linkElement).toBeInTheDocument();
});
