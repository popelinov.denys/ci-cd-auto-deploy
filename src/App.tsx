import React from "react";
import hoba from "./img/hoba.jpeg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={hoba} className="App-logo" alt="logo" />
        <p>
          Хоба <code>CI\CD</code> на {process.env.REACT_APP_NODE_ENV} env.
        </p>
      </header>
    </div>
  );
}

export default App;
